import fetch from 'isomorphic-fetch'


const fetchData = async () => {
  try {
    const response = await fetch('http://localhost:3333/data')
    if (response.status >= 400) {
      throw new Error("Bad response from server");
    }
    return await response.json();
  } catch {
    console.log('error in fetching data from server')
  }
}

export default  fetchData;
