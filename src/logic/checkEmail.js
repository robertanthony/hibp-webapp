import fetch from 'isomorphic-fetch'


const checkEmail = async (name,surname,email) => {
  try {

    const requestOptions = {
      method: 'POST',
      headers: { 'Content-Type': 'application/json' },
      body: JSON.stringify({ name,surname,email})
    };

    const response = await fetch('http://localhost:3333/email',requestOptions)
    if (response.status >= 400) {
      throw new Error("Bad response from server");
    }
  } catch {
    console.log('error in sending email check to  server')
  }
}

export default  checkEmail;

