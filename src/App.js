import React from 'react';
import HibpData from './features/hibp/Hibp';
import Email from './features/email/Email';

import './main.css';

const App = () => {
  return (
    <div className="main">
      <h1 className="main_title">Are you pwnd?</h1>

      <div className="App_main">
        <Email/>
        <HibpData/>
      </div>
    </div>
  );
}

export default App;
