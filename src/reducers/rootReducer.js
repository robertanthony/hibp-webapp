import { combineReducers } from 'redux'
import hibpData from '../features/hibp/hibpSlice'

export default combineReducers({
  hibpData
})
