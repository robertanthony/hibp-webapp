import React, {useState} from 'react';
import checkEmail from '../../logic/checkEmail'

const Email = () => {


  const [name, setName] = useState('');
  const [surname, setSurname] = useState('');
  const [email, setEmail] = useState('');

  const handleNameChange = (e) => {
    setName(e.target.value)
  }
  const handleSurnameChange = (e) => {
    setSurname(e.target.value)
  }
  const handleEmailChange = (e) => {
    setEmail(e.target.value)
  }

  const handleSubmit = (e) => {
    e.preventDefault()
    if (name && surname && email) {
      checkEmail(name, surname, email)
      setName('')
      setSurname('')
      setEmail('')
    }
  }

  return (
    <div className="email_main">
      <h2>Check your email</h2>
      <form onSubmit={handleSubmit} className="email_form">
        <input placeholder="Name" value={name}
               onChange={handleNameChange}/>
        <input placeholder="Surname" value={surname}
               onChange={handleSurnameChange}/>
        <input placeholder="Email" value={email}
               onChange={handleEmailChange}/>
        <button>Submit</button>
      </form>
    </div>
  )
}

export default Email;
