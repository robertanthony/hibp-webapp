import React, {useEffect} from 'react';
import {useSelector, useDispatch} from 'react-redux';
import {
  dataSelector,
  getHibpData
} from './hibpSlice';


const Hibp = () => {
  const dispatch = useDispatch()
  const data = useSelector(dataSelector)


  useEffect(() => {
    const timer = setInterval(() => {
      dispatch(getHibpData())
    }, 2000);
    return () => clearInterval(timer);
  });

  return (
    <div className="hibp_main">
      <table className="hibp_table">
        <thead>
        <tr>
          <th>Name</th>
          <th>Surname</th>
          <th>Email</th>
          <th>Is Breached?</th>
        </tr>
        </thead>
        <tbody>
        {data && data.length && data.map((datum,i) => <tr key={i}>
          <td>{datum.name}</td>
          <td>{datum.surname}</td>
          <td>{datum.email}</td>
          <td>{datum.isBreached && "😱"}</td>
        </tr>)}
        </tbody>
      </table>
    </div>
  );
}

export default Hibp;
