import { createSlice,createAsyncThunk } from '@reduxjs/toolkit';
import getData from '../../logic/fetchData.js'



export const getHibpData = createAsyncThunk(
  'hibp/getHibpData',
  async (_, thunkAPI) => {
   return await getData()
  }
)


export const hibpSlice = createSlice({
  name: 'hibp',
  initialState: {
    hibpData:[]
  },
  reducers: {

  },
  extraReducers: {
    // Add reducers for additional action types here, and handle loading state as needed
    [getHibpData.fulfilled]: (state, action) => {
      state.hibpData = action.payload
    }
  }
});

export const { refreshReceived } = hibpSlice.actions;




export const dataSelector = state => state.hibpData.hibpData;

export default hibpSlice.reducer;
